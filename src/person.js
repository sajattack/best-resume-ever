/* eslint-disable */
export const PERSON = {
  name: {
    first: 'Paul',
    last: 'Sajna',
  },
  position: 'Software Developer',
  birth: {
    year: 1996,
    location: 'Kelowna'
  },
  experience: [{
      company: 'Boomers Computers',
      position: 'Computer Technician',
      timeperiod: 'August 2016-January 2017',
      description: 'Troubleshot, repaired, and built desktop and laptop computers'
    },
    {
      company: 'Valley Laptop',
      position: 'Laptop Technician/Website Administrator',
      timeperiod: '2014-2016',
      description: 'Refurbished laptops, offered technical support and built an ecommerce website for the company'
    }
  ],
  projects:[{
      name: 'Throw Remote',
      timeperiod: 'February 2016',
      description: 'Remote Control app for Throw streaming media player'
  },
  {
      name: 'Emoji Rehab',
      timeperiod: 'March 2016',
      description: 'Community notice board for Victoria. Has the capability of marking locations with emoji.'
  },
  {
      name: 'Down2Home',
      timeperiod: 'June 2013',
      description: 'Saves mobile bandwidth by sending download links to a remote PC'
  },
  {
      name: 'Net Toolbox',
      timeperiod: 'April 2013',
      description: 'A suite of networking tools for professionals and enthusiasts.'
  },
  ],
  education: [
    {
      degree: 'Information and Computer Systems Diploma',
      timeperiod: 'Sept 2017 - April 2019',
      description: 'Camosun College - Victoria, BC, Canada'
    }
  ],
  // skill level goes 0 to 100
  skills: [{
      name: 'Java',
      level: '80'
    },
    {
      name: 'Android SDK',
      level: '90'
    },
    {
      name: 'Python',
      level: '90'
    },
    {
      name: 'Go',
      level: '75'
    },
    {
      name: 'C++',
      level: '60'
    },
    {
      name: 'Node.js',
      level: '70'
    },
    {
      name: 'Linux',
      level: '80'
    },
    {
      name: 'AWS',
      level: '80'
    }
  ],
  contact: {
    email: 'paulsajna@gmail.com',
    phone: '+1 250 891 4058',
    street: '4402 Wilkinson Rd',
    city: 'Victoria',
    website: 'paulsajna.com',
    github: 'sajattack'
  }
};
